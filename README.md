# Introduction

We'll be using Python and OpenCV to process frames captured by the camera. Also, to increase frame rate we 'll be using different thread processes to read and process frames. Let's start installing dependencies.

# Acknowledgements

Special thanks to Adrian Rosebrock for the extremely helpful series of tutorials on Computer Vision at https://www.pyimagesearch.com

# Install OpenCV

## Install dependencies for OpenCV

```bash
$ sudo apt-get install build-essential cmake pkg-config
$ sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
$ sudo apt-get install libxvidcore-dev libx264-dev
$ sudo apt-get install libgtk2.0-dev libgtk-3-dev
$ sudo apt-get install python2.7-dev python3-dev
```

## Install the latest version of pip

```bash
$ wget https://bootstrap.pypa.io/get-pip.py
$ sudo python get-pip.py
$ sudo python3 get-pip.py
```

## Use virtualenv

First, it’s important to understand that a virtual environment is a special tool used to keep the dependencies required by different projects in separate places by creating isolated, independent Python environments for each of them. In short, it solves the “Project X depends on version 1.x, but Project Y needs 4.x” dilemma. It also keeps your global `site-packages` neat, tidy, and free from clutter.

It’s standard practice in the Python community to be using virtual environments of some sort, so I highly recommend that you do the same:

```bash
$ sudo pip install virtualenv virtualenvwrapper
$ sudo rm -rf ~/.cache/pip
```
Now that both `virtualenv` and `virtualenvwrapper` have been installed, we need to update our `~/.profile` file to include the some lines at the bottom of the file. I will be using `vim` to edit files, which is not installed in the default Raspbian installation, so if you want to use the same editor just install it with:

```bash
sudo apt-get install vim
```
Now let's edit the profile.
```bash
vim ~/.profile
```
Add the following lines at the end of the file:
```bash
# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh
```
You can now force a reload of your `~/.profile` file by using the source command:
```bash
source ~/.profile
```

## Create a Python virtual environment

Next, let’s create a Python virtual environment that we’ll be using for computer vision development:

```bash
$ mkvirtualenv cv -p python2
```
This command will create a new Python virtual environment named `cv` using Python 2.7.
If you instead want to use Python 3, you’ll want to use this command instead:
```bash
$ mkvirtualenv cv -p python3
```
Now the `cv` Python virtual environment is entirely independent and sequestered from the default Python version included in the download of Raspbian Stretch. Any Python packages in the global `site-packages` directory will not be available to the `cv` virtual environment. Similarly, any Python packages installed in `site-packages` of `cv` will not be available to the global install of Python. Keep this in mind when you’re working in your Python virtual environment and it will help avoid a lot of confusion and headaches.

If you ever reboot your Raspberry Pi; log out and log back in; or open up a new terminal, you’ll need to use the `workon` command to re-access the `cv` virtual environment: 
```bash
$ source ~/.profile
$ workon cv
```
To validate and ensure you are in the `cv` virtual environment, examine your command line — if you see the text *(cv)* preceding your prompt, then you are in the `cv` virtual environment, if not just execute the commands above.

Assuming you’ve made it this far, you should now be in the `cv` virtual environment (which you should stay in for the rest of the OpenCV installation step). Our only Python dependency is `NumPy`, a Python package used for numerical processing (this will take some time):
```bash
$ pip install numpy
```

## Download and extract OpenCV library and contrib

At the time of this guide I installed OpenCV version 3.3.0. Keep this in mind if you need to install a newer version (replace 3.3.0 with your version)

```bash
$ cd ~
$ wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.3.0.zip
$ unzip opencv.zip
$ wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.3.0.zip
$ unzip opencv_contrib.zip
```
## Build and Install OpenCV

Make sure that you are working in the `cv` virtual environment:
```bash
$ workon cv
```
### Configure the build

Now do the following:
```bash
$ cd ~/opencv-3.3.0/
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.3.0/modules \
    -D BUILD_EXAMPLES=ON ..
```

Before we move on to the actual compilation step, make sure you examine the output of CMake.
Start by scrolling down the section titled Python 2 (or Python 3).

If you are compiling OpenCV 3 for Python 2.7, then make sure your Python 2 section includes valid paths to the Interpreter, Libraries, `numpy` and packages path. Notice how the Interpreter points to our python2.7 binary located in the `cv` virtual environment. The `numpy` variable also points to the `NumPy` installation in the `cv` environment. Similarly, if you’re compiling OpenCV for Python 3, make the observations mentioned for the Python 3 section.

### Adjust the swap file size

Before you start the compile process, you should increase your swap space size. This enables OpenCV to compile with all four cores of the Raspberry PI without the compile hanging due to memory problems.

Open your `/etc/dphys-swapfile` with the vim editor.

```bash
sudo vim /etc/dphys-swapfile
```
and then edit the `CONF_SWAPSIZE` variable:
```bash
# set size to absolute value, leaving empty (default) then uses computed value
# you most likely don't want this, unless you have an special disk situation
# CONF_SWAPSIZE=100
CONF_SWAPSIZE=1024
```
Notice that I’ve commented out the 100MB line and added a 1024MB line. This is the secret to compile with multiple cores on the Raspbian Stretch.

**If you skip this step, OpenCV might not compile.**

To activate the new swap space, restart the swap service:
```bash
$ sudo /etc/init.d/dphys-swapfile stop
$ sudo /etc/init.d/dphys-swapfile start
```
**Note:** It is possible to burn out the Raspberry Pi microSD card because flash memory has a limited number of writes until the card won’t work. It is highly recommended that you change this setting back to the default when you are done compiling and testing the install.

### Compile OpeCV

Finally, we are now ready to compile OpenCV:

```bash
$ cd ~/opencv-3.3.0/build
$ make -j4
```
This step is going to take a LOT of time, so you may need to go for a walk and stretch your legs (*estimated compile time is ~2h*).
After a successful compilation (progress indicator: 100%) no errors should be mentioned.

### Install OpenCV

From there, all you need to do is install OpenCV 3 on your Raspberry Pi 3:
```bash
$ sudo make install
$ sudo ldconfig
```
### Verify the OpenCV Installation

We’re almost done — just a few more steps to go and you’ll be ready to use your Raspberry Pi 3 with OpenCV 3 on Raspbian Stretch.

#### For Python 2.7

Provided that compilation finished without error, OpenCV should now be installed in `/usr/local/lib/python2.7/site-packages`. You can verify this using the `ls` command:
```bash
$ ls -l /usr/local/lib/python2.7/site-packages/
total 3880
-rw-r--r-- 1 root staff 3971680 Jul 22 03:26 cv2.so
```
**Note:** In some cases, OpenCV can be installed in `/usr/local/lib/python2.7/dist-packages` (note the `dist-packages` rather than `site-packages`). If you do not find the `cv2.so` bindings in `site-packages`, be sure to check `dist-packages`.
Our final step is to sym-link the OpenCV bindings into our `cv` virtual environment for Python 2.7:
```bash
$ cd ~/.virtualenvs/cv/lib/python2.7/site-packages/
$ ln -s /usr/local/lib/python2.7/site-packages/cv2.so cv2.so
```
#### For Python 3.5

After running `make install`, your OpenCV + Python bindings should be installed in `/usr/local/lib/python3.5/site-packages`. Again, you can verify this with the `ls` command:
```bash
$ ls -l /usr/local/lib/python3.5/site-packages/
total 3876
-rw-r--r-- 1 root staff 3968472 Jul 22 03:27 cv2.cpython-35m-arm-linux-gnueabihf.so
```
Notice that the output `.so` file is named `cv2.cpython-35m-arm-linux-gnueabihf.so` (or some variant of) rather than simply `cv2.so` (like in the Python 2.7 bindings). I don’t know why this happens, propably due to a bug in the CMake script, when compiling the OpenCV 3 bindings for Python 3+.
All we need to do is rename the file:
```bash
$ cd /usr/local/lib/python3.5/site-packages/
$ sudo mv cv2.cpython-35m-arm-linux-gnueabihf.so cv2.so
```
After renaming to `cv2.so`, we can sym-link our OpenCV bindings into the `cv` virtual environment for Python 3.5:
```bash
$ cd ~/.virtualenvs/cv/lib/python3.5/site-packages/
$ ln -s /usr/local/lib/python3.5/site-packages/cv2.so cv2.so
```

### Test the OpenCV Installation

Finally, verify that your OpenCV installation is working properly.
Open up a new terminal, execute the `source` and `workon` commands, and then finally attempt to import the Python + OpenCV bindings:
```bash
$ source ~/.profile 
$ workon cv
$ python
>>> import cv2
>>> cv2.__version__
'3.3.0'
```
If you can see a version number for OpenCV 3 as above, then it has been successfully installed on your Raspberry Pi 3 Python environment.

### Clean-up

Once OpenCV has been installed, you can remove both the `opencv-3.3.0` and `opencv_contrib-3.3.0` directories to free up some space on your disk. However, be cautious with this command! Make sure OpenCV has been properly installed on your system before blowing away these directories. A mistake here could cost you hours in compile time.

```bash
$ rm -rf opencv-3.3.0 opencv_contrib-3.3.0
```

Reset the swap size:
```bash
$ sudo vim /etc/dphys-swapfile
```
Reset the `CONF_SWAPSIZE` variable:
```bash
# set size to absolute value, leaving empty (default) then uses computed value
# you most likely don't want this, unless you have an special disk situation
CONF_SWAPSIZE=100
# CONF_SWAPSIZE=1024
```
If you skip this step, your memory card won’t last as long. As stated above, larger swap spaces may lead to memory corruption, so I recommend setting it back to 100MB.

To revert to the smaller swap space, restart the swap service:
```bash
$ sudo /etc/init.d/dphys-swapfile stop
$ sudo /etc/init.d/dphys-swapfile start
```
Congrats! You have a brand new, fresh install of OpenCV on your Raspberry Pi.

# Install other python packages 

```bash
$ source ~/.profile 
$ workon cv
```

```bash
$ pip install imutils
```
or if it is already install upgrade:
```bash
$ pip install --upgrade imutils
```

# Execute the demo scripts

```bash
$ python pidemo.py --threading 0
$ python pidemo.py --threading 1

```

