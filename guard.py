# import the necessary packages
from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2

# ----------------------------------------------------------------------------
# construct the argument parser and parse the arguments
parser = argparse.ArgumentParser()

parser.add_argument("-d", "--display", type=int, default=0,
	help="Whether or not frames should be displayed")
parser.add_argument("-m", "--motion", type=int, default=1,
	help="Whether or not motion detection will be used to capture frames")
parser.add_argument("-v", "--video",
        help="path to the video file")
parser.add_argument("-a", "--area", type=int, default=500,
        help="minimum area size for motion detection")

args = vars(parser.parse_args())

# FIXME: First frame should be static scene (no moving objects, shadows)
# ----------------------------------------------------------------------------
def captureFromCam(args):
    
    vs = VideoStream(src=0).start()
    time.sleep(2.0)
    # initialize the first frame in the video stream
    firstFrame = None
    # loop over the frames of the video

    while True:
	# grab the current frame and initialize the occupied/unoccupied text
	frame = vs.read()
	frame = frame
	text = "Unoccupied"
	
        # if the frame isn't grabbed, then we've reached the end of the video
	if frame is None:
	    break
	
        # resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)
	
        # if the first frame is None, initialize it
	if firstFrame is None:
            firstFrame = gray
	    continue

	# compute the absolute diff between the current frame and first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
 
	# dilate the thresholded image to fill in holes
	thresh = cv2.dilate(thresh, None, iterations=2)
        # find contours on thresholded image
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
 
	# loop over the contours
	for c in cnts:
	    # if the contour is too small, ignore it
	    if cv2.contourArea(c) < args["area"]:
		continue
	    # compute the bounding box for the contour,
            # draw it on the frame and update the text
	    (x, y, w, h) = cv2.boundingRect(c)
	    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
	    text = "Occupied"

	# draw the text and timestamp on the frame
	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame,
                datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
		(10, frame.shape[0] - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
 
	# show the frame and record if the user presses a key
        if (args['display'] != 0):
            cv2.imshow("Security Feed", frame)
	    cv2.imshow("Thresh", thresh)
            cv2.imshow("Frame Delta", frameDelta)

	key = cv2.waitKey(1) & 0xFF
 
	# if the `q` key is pressed, break from the loop
	if key == ord("q"):
	    break
 
    # cleanup the camera and close any open windows
    vs.stop()
    cv2.destroyAllWindows()

# ----------------------------------------------------------------------------
def captureFromVideo(args):

    vs = cv2.VideoCapture(args["video"])
    # initialize the first frame in the video stream
    firstFrame = None
    # loop over the frames of the video

    while True:
	# grab the current frame and initialize the occupied/unoccupied text
	frame = vs.read()
	frame = frame[1]
	text = "Unoccupied"

	# if the frame isn't grabbed, then we've reached the end of the video
	if frame is None:
	    break

	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	# if the first frame is None, initialize it
	if firstFrame is None:
            firstFrame = gray
	    continue 

	# compute the absolute diff between the current frame and first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
 
	# dilate the thresholded image to fill in holes
	thresh = cv2.dilate(thresh, None, iterations=2)
        # find contours on thresholded image
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
 
	# loop over the contours
	for c in cnts:
	    # if the contour is too small, ignore it
	    if cv2.contourArea(c) < args["area"]:
		continue
	    # compute the bounding box for the contour,
            # draw it on the frame and update the text
	    (x, y, w, h) = cv2.boundingRect(c)
	    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
	    text = "Occupied"

	# draw the text and timestamp on the frame
	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame,
                datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
		(10, frame.shape[0] - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
 
	# show the frame and record if the user presses a key
        if (args['display'] != 0):
            cv2.imshow("Security Feed", frame)
	    cv2.imshow("Thresh", thresh)
            cv2.imshow("Frame Delta", frameDelta)

	key = cv2.waitKey(1) & 0xFF
 
	# if the `q` key is pressed, break from the loop
	if key == ord("q"):
	    break
 
    # cleanup and close any open windows
    vs.release()
    cv2.destroyAllWindows()

# ----------------------------------------------------------------------------
def run(args):
    # if the video argument is None, then we are reading from webcam
    if args.get("video", None) is None:
        captureFromCam(args)
    # otherwise, we are reading from a video file
    else:
        captureFromVideo(args)

# ----------------------------------------------------------------------------
# EXEC
# ----------------------------------------------------------------------------
run(args)

# TODO OpenCV Saliency Detection: https://www.pyimagesearch.com/2018/07/16/opencv-saliency-detection/
# TODO Face clustering with Python: https://www.pyimagesearch.com/2018/07/09/face-clustering-with-python/
# TODO Raspberry Pi Face Recognition: https://www.pyimagesearch.com/2018/06/25/raspberry-pi-face-recognition/

